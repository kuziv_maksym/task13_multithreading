package com.epam;

import com.epam.fib.ControllerFib;
import com.epam.locking.ControllerLock;
import com.epam.pingpong.ControllerPing;

public class Application {
    public static void main(String[] args) throws InterruptedException {
        new ControllerPing().startThreads();
        new ControllerFib().show();
        new ControllerLock().startProgram();
    }
}

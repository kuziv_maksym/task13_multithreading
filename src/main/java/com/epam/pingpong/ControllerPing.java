package com.epam.pingpong;

public class ControllerPing {
    NotifyWait notifyWait;
    public ControllerPing() {
        notifyWait = new NotifyWait();
    }

    public void startThreads() throws InterruptedException {
        notifyWait.thread1();
        Thread.sleep(2000);
        notifyWait.thread2();
        Thread.sleep(2000);
        notifyWait.thread3();
        Thread.sleep(2000);
        System.out.println("------------------------------------");
    }
}

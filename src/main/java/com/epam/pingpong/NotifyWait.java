package com.epam.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NotifyWait {
    public static Logger logger = LogManager.getLogger(NotifyWait.class);
    Object sync = new Object();

    void thread1() {
        Thread thread = new Thread(() -> {
            synchronized (sync) {
                System.out.println(Thread.currentThread().getName() + " Ready to wait..");
                try {
                    sync.wait();
                    System.out.println("wainting...");
                } catch (InterruptedException e) {
                    logger.error("Error for syncronized thread");
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + " Notify ? OK - continue");
        });
        thread.start();
    }

    void thread2() {
        Thread thread = new Thread(() -> {
           synchronized (sync) {
               System.out.println(Thread.currentThread().getName() + " Ready to wait..");
               try {
                   sync.wait();
                   System.out.println("waiting..");
               } catch (InterruptedException e) {
                   logger.error("Error for syncronized thread");
                   e.printStackTrace();
               }
           }
            System.out.println(Thread.currentThread().getName() + " Notify ? OK - continue");
        });
        thread.start();
    }

    void thread3(){
        Thread thread= new Thread(() -> {
           synchronized (sync) {
               System.out.println(Thread.currentThread().getName() + " ready to notify...");
               sync.notifyAll();
               System.out.println(Thread.currentThread().getName() + " notified all...");
           }
        });
        thread.start();
    }
}

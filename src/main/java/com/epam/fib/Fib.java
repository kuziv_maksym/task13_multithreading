package com.epam.fib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Fib {
    public static Logger logger = LogManager.getLogger(Fib.class);
    static long sum = 0;
    Object sync = new Object();

    class MyThread extends Thread {
        int n;


        public MyThread(int n) {
            this.n = n;
        }

        @Override
        public void run() {
            for (int i = 0; i < n; i++) {
                synchronized (sync) {
                    sum = sum + fibonachi(n);
                }
            }
            System.out.println(Thread.currentThread().getName() + " finish with sum " + sum);
        }
    }

    static int fibonachi(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        } else {
            return fibonachi(n - 1) + fibonachi(n - 2);
        }
    }

    public void show() {
        MyThread myThread = new MyThread(3);
        MyThread myThread2 = new MyThread(3);
        MyThread myThread3 = new MyThread(3);
        myThread.start();
        myThread2.start();
        myThread3.start();

        try {
            myThread.join();
            myThread3.join();
            myThread2.join();

        } catch (InterruptedException e) {
            logger.error("Error join theads");
            e.printStackTrace();

        }
        System.out.println(sum);
    }
}

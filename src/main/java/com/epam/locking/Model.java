package com.epam.locking;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Model {
    public static Logger logger = LogManager.getLogger(Model.class);
    private static ReadWriteLock rw = new ReentrantReadWriteLock();
    private static Lock readLock = rw.readLock();
    private static Lock writeLock = rw.writeLock();

    public static void outer() {
        writeLock.lock();
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
        inner();
        writeLock.unlock();
    }

    private static void inner() {
        readLock.lock();
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            logger.trace("Interrupt thread");
        }
        readLock.unlock();
    }
}

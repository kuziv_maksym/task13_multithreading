package com.epam.locking;

public class ControllerLock {
    Model model;
    public ControllerLock() {
        model = new Model();
    }

    public void startProgram() {
        Thread t1 = new Thread(() -> Model.outer() );
        Thread t2 = new Thread(() -> Model.outer());
        t1.start();
        t2.start();
    }
}
